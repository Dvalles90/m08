package cat.xtec.ioc.m08;

import java.util.Scanner;
import java.lang.Math;

public class M08 {

    //David Valles Sendiu
    //He aprés molt fent el mòdul 8 de DAW encara que hagi estat dur!!!
    
    
	private static Scanner jugador;
	static final int PIEDRA = 1;
	static final int PAPEL = 2;
	static final int TIJERAS = 3;

	public static void main(String[] args) {
		int victorias = 0;
		int derrotas = 0;
		int empates = 0;
		int eleccioMenu = 0;
		boolean allok = true;

		do {

			mostrarMenu();

			jugador = new Scanner(System.in);
			eleccioMenu = jugador.nextInt();

			if (eleccioMenu == 1) {
				System.out.println("");
				System.out.println("Piedra = 1, papel = 2 o tijeras = 3");
				System.out.println("Escoje una opcion");

				jugador = new Scanner(System.in);
				int eleccioJugador = jugador.nextInt();

				int eleccioMaquina = getEleccioMaquina(1, 3);
				System.out.println("La maquina ha escojido " + eleccioMaquina);
				if (eleccioJugador == eleccioMaquina) {
					System.out.println("Empate.");
					empates++;
				} else if (eleccioJugador == PIEDRA && eleccioMaquina == TIJERAS
						|| eleccioJugador == TIJERAS && eleccioMaquina == PAPEL
						|| eleccioJugador == PAPEL && eleccioMaquina == PIEDRA) {
					System.out.println("El jugador gana.");
					victorias++;
				} else {
					System.out.println("La maquina gana.");
					derrotas++;
				}

			} else if (eleccioMenu == 2) {
				estadisticas(victorias, derrotas, empates);

			} else {
				allok = false;
				System.out.println("Saliendo...");
			}

			System.out.println("");

		} while (allok);
	}

	public static int getEleccioMaquina(int min, int max) {
		min = (int) Math.ceil(min);
		max = (int) Math.floor(max);
		return (int) Math.floor(Math.random() * (max - min + 1) + min);
	}

	public static void mostrarMenu() {
		System.out.println("--------------------------");
		System.out.println("  PIEDRA, PAPEL, TIJERAS");
		System.out.println("--------------------------");
		System.out.println("");
		System.out.println("1. Jugar");
		System.out.println("2. Estadisticas");
		System.out.println("");
		System.out.println("3. Salir");
		System.out.println("");
		System.out.println("Escoje una opcion");

	}

	public static void estadisticas(int victorias, int derrotas, int empates) {
		System.out.println("Victorias = " + victorias);
		System.out.println("Derrotas = " + derrotas);
		System.out.println("Empates = " + empates);

	}
}